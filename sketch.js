
var myPlants = []; // array of objects

function setup() {
  createCanvas(800, 200);

  var rowPos = 100;
  var startPos = 100;

//  myPlants[0] = new CharPlant("D");
  myPlants.push(new CharPlant("D", startPos + 20, rowPos));
  myPlants.push(new CharPlant("E", startPos + 40, rowPos));
  myPlants.push(new CharPlant("V", startPos + 60, rowPos));
  myPlants.push(new CharPlant(".", startPos + 80, rowPos));
  myPlants.push(new CharPlant("O", startPos + 100, rowPos));
  myPlants.push(new CharPlant("L", startPos + 120, rowPos));
  myPlants.push(new CharPlant("U", startPos + 140, rowPos));
  myPlants.push(new CharPlant("T", startPos + 160, rowPos));
  myPlants.push(new CharPlant("I", startPos + 180, rowPos));
  myPlants.push(new CharPlant("O", startPos + 200, rowPos));
  myPlants.push(new CharPlant("N", startPos + 220, rowPos));

  for(var i = 0; i < myPlants.length; i++) {
    myPlants[i].preGrow();
  }

}

function draw() {
  background(50);
  strokeWeight(2);

  for(var i = 0; i < myPlants.length; i++) {
    myPlants[i].grow();
    myPlants[i].display();
  }
}


function mouseClicked() {
  for(var i = 0; i < myPlants.length; i++) {
    if (myPlants[i].mouseOver() && myPlants[i].isGrown){

      myPlants[i].isGrown = false;
      myPlants[i].size = 1;
      myPlants[i].x = myPlants[i].x + random(-10, 10);
      myPlants[i].y = myPlants[i].y + random(-10, 10);

      var newPlants = random(0, 3);
      for(var ii = 0; ii < newPlants; ii++) {
        myPlants.push(new CharPlant(myPlants[i].char, myPlants[i].x + random(-20, 20), myPlants[i].y + random(-20, 20)));
      }
    }
  }
}



// class
function CharPlant(_char, _xPos, _yPos) {
  this.char = _char;
  this.x = _xPos;
  this.y = _yPos;
  this.maxSize = random(30, 40);
  this.size = 1;
  this.c = color(random(0,255), random(0,255), random(0,255));
  this.growthSpeed = 0.1; // per second
  this.isGrown = false;

  this.preGrow = function() {
    this.size = random(this.maxSize/2, this.maxSize);
  };

  this.display = function() {
    fill(this.c);
    textSize(this.size);
    textAlign(CENTER);
    text (this.char, this.x, this.y);
  };

  this.grow = function() {

    if (!this.isGrown){


    var g = (1.0/frameRate())*this.growthSpeed;
    if (g == "Infinity"){
      g = 0;
    }

    if (this.mouseOver()){
      g = g * 15;
    }

    this.size = this.size + g;

    if (this.size > this.maxSize){
      this.isGrown = true;
    }
  }
  };

  this.mouseOver = function(){
    var r = false;
    var d = dist(mouseX, mouseY, this.x, this.y);
    if (d < 10){
      r = true;
    }
    return r;
  };
}
